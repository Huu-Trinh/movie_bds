package com.bds.bds;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdsBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdsBeApplication.class, args);
	}

}
