package com.bds.bds.config;

import com.bds.bds.model.AbstractAuditingEntity;
import com.bds.bds.security.ServiceUtil;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.sql.Connection;
import java.time.Instant;
import java.util.Properties;

@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class}),
        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})
})
public class AuditInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        ServiceUtil serviceUtil = new ServiceUtil();
        Object parameter = invocation.getArgs();


        if (parameter instanceof AbstractAuditingEntity) {
            AbstractAuditingEntity entity = (AbstractAuditingEntity) parameter;
            if ("update".equals(invocation.getMethod().getName())) {
                entity.setLastModifiedDate(Instant.now());
                // set author
                entity.setLastModifiedBy(serviceUtil.getCurrentUserLogin().orElse("system"));
            } else if ("insert".equals(invocation.getMethod().getName())) {
                entity.setCreatedDate(Instant.now());
                entity.setLastModifiedDate(Instant.now());
                // set author
                entity.setCreatedBy(serviceUtil.getCurrentUserLogin().orElse("system"));
            }
        }

        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
