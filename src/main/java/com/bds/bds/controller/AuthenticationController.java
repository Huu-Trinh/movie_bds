package com.bds.bds.controller;

import com.bds.bds.model.dto.request.RegisterRequestDTO;
import com.bds.bds.model.dto.response.LoginDTO;
import com.bds.bds.model.dto.request.LoginRequestDTO;
import com.bds.bds.model.dto.response.RegisterResponseDTO;
import com.bds.bds.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<LoginDTO> login(@RequestBody LoginRequestDTO request) {
        return new ResponseEntity<LoginDTO>(authenticationService.login(request), HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<RegisterResponseDTO> register(@RequestBody RegisterRequestDTO request) {
        return new ResponseEntity<RegisterResponseDTO>(authenticationService.register(request), HttpStatus.OK);
    }
}
