package com.bds.bds.mapper;

import com.bds.bds.model.Token;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TokenMapper {
    List<Token> findAllByUsername(String username);

    Optional<Token> findByToken(String token);

    void saveAll(List<Token> tokens);

    int saveToken(Token token);
}
