package com.bds.bds.mapper;

import com.bds.bds.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface UserMapper {
    // Insert a user, with SQL provided directly in the annotation
    Integer login(User user);

    Integer insert(User user);

    User findByUsername(String username);

    // Update a user, SQL in XML configuration
    Integer updateUser(User user);

    // Delete a user
    Integer deleteUser(Long id);

    // Find a user by ID
    User findUserById(Long id);

    // List all users
    List<User> findAllUsers();

    // Find a user by email
    Optional<User> findByEmail(String email);

    // Find a user by username with status 0
    Optional<User> findByUsernameAndDeletedFalse(String username);
}
