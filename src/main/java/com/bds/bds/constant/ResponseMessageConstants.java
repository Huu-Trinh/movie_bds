package com.bds.bds.constant;

public class ResponseMessageConstants {
    public final static String USERNAME_NOT_FOUND = "Username not found in the system";
    public final static String PASSWORD_NOT_CORRECT = "Password is not correct";
    public final static String REVOKE_TOKEN_FAILED = "Revoke token failed";
    public final static String LOGIN_FAILED = "Login failed";
    public final static String REGISTER_FAILED = "Register failed";
    public final static String USERNAME_EXISTED = "Username is already existed";
    public final static String EMAIL_EXISTED = "Email is already existed";
    public final static String TOKEN_NOT_FOUND = "Token not found";
}
