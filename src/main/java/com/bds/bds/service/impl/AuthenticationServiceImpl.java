package com.bds.bds.service.impl;

import com.bds.bds.constant.ResponseMessageConstants;
import com.bds.bds.enums.TokenType;
import com.bds.bds.enums.UserRole;
import com.bds.bds.exception.AccessDeniedException;
import com.bds.bds.mapper.TokenMapper;
import com.bds.bds.mapper.UserMapper;
import com.bds.bds.model.Token;
import com.bds.bds.model.User;
import com.bds.bds.model.dto.request.RegisterRequestDTO;
import com.bds.bds.model.dto.response.LoginDTO;
import com.bds.bds.model.dto.request.LoginRequestDTO;
import com.bds.bds.model.dto.UserDTO;
import com.bds.bds.model.dto.response.RegisterResponseDTO;
import com.bds.bds.security.JwtUtil;
import com.bds.bds.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserMapper userMapper;
    private final JwtUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final TokenMapper tokenMapper;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Optional<User> findByEmail(String email) {
        return userMapper.findByEmail(email);
    }

    @Override
    public LoginDTO login(LoginRequestDTO loginRequest) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
        );
        Optional<User> account = userMapper.findByUsernameAndDeletedFalse(loginRequest.getUsername());
        account.orElseThrow(
                () -> new AccessDeniedException(ResponseMessageConstants.USERNAME_NOT_FOUND)
        );
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(authenticationToken);
        } catch (RuntimeException e) {
            throw new AccessDeniedException(ResponseMessageConstants.PASSWORD_NOT_CORRECT);
        }

        String accessToken = jwtUtil.generateToken(authentication);
        String refreshToken = jwtUtil.generateRefreshToken(authentication);
        revokeAllUserTokens(authentication);
        saveUserToken(authentication, accessToken);
        saveUserRefreshToken(authentication, refreshToken);
        UserDTO userDTO = modelMapper.map(account.get(), UserDTO.class);

        return new LoginDTO(accessToken, refreshToken, userDTO);
    }

    @Override
    public RegisterResponseDTO register(RegisterRequestDTO registerRequestDTO) {
        User user = modelMapper.map(registerRequestDTO, User.class);
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        user.setRole(UserRole.USER);
        user.setStatus(1);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // is email existed
        Optional<User> existedEmail = userMapper.findByEmail(registerRequestDTO.getEmail());
        existedEmail.ifPresent(u -> {
            throw new AccessDeniedException(ResponseMessageConstants.EMAIL_EXISTED);
        });

        // is username existed
        Optional<User> existedUsername = userMapper.findByUsernameAndDeletedFalse(registerRequestDTO.getUsername());
        existedUsername.ifPresent(_ -> {
            throw new AccessDeniedException(ResponseMessageConstants.USERNAME_EXISTED);
        });

        try {
            Integer numberOfUser = userMapper.insert(user);
            System.out.println("User register: " + numberOfUser);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new AccessDeniedException(ResponseMessageConstants.REGISTER_FAILED);
        }

        String accessToken = jwtUtil.generateToken(user);
        String refreshToken = jwtUtil.generateRefreshToken(user);

        return new RegisterResponseDTO(true, accessToken, refreshToken, userDTO);
    }

    private void revokeAllUserTokens(Authentication authentication) {
        List<Token> validUserTokens = tokenMapper.findAllByUsername(authentication.getName());
        if (validUserTokens.isEmpty()) {
            return;
        }
        validUserTokens.forEach(token -> {
            token.setRevoked(true);
            token.setExpired(true);
        });
        tokenMapper.saveAll(validUserTokens);
    }

    private void revokeAllUserTokens(User user) {
        List<Token> validUserTokens = tokenMapper.findAllByUsername(user.getUsername());
        if (validUserTokens.isEmpty()) {
            return;
        }
        validUserTokens.forEach(token -> {
            token.setRevoked(true);
            token.setExpired(true);
        });
        try {
            tokenMapper.saveAll(validUserTokens);
        } catch (Exception e) {
            throw new AccessDeniedException(ResponseMessageConstants.REVOKE_TOKEN_FAILED);
        }
    }

    private void saveUserToken(Authentication authentication, String accessToken) {
        User user = userMapper.findByUsernameAndDeletedFalse(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        Token token = new Token();
        token.setUser(user);
        token.setToken(accessToken);
        token.setTokenType(TokenType.BEARER);
        token.setExpired(false);
        token.setRevoked(false);
        try {
            tokenMapper.saveToken(token);
        } catch (Exception e) {
            throw new AccessDeniedException(ResponseMessageConstants.LOGIN_FAILED);
        }
    }

    private void saveUserRefreshToken(Authentication authentication, String accessToken) {
        User user = userMapper.findByUsernameAndDeletedFalse(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        Token token = new Token();
        token.setUser(user);
        token.setToken(accessToken);
        token.setTokenType(TokenType.REFRESH);
        token.setExpired(false);
        token.setRevoked(false);
        try {
            tokenMapper.saveToken(token);
        } catch (Exception e) {
            throw new AccessDeniedException(ResponseMessageConstants.LOGIN_FAILED);
        }
    }


}
