package com.bds.bds.service;

import com.bds.bds.model.User;
import com.bds.bds.model.dto.request.RegisterRequestDTO;
import com.bds.bds.model.dto.response.LoginDTO;
import com.bds.bds.model.dto.request.LoginRequestDTO;
import com.bds.bds.model.dto.response.RegisterResponseDTO;

import java.util.Optional;

public interface AuthenticationService {
    Optional<User> findByEmail(String email);

    LoginDTO login(LoginRequestDTO loginRequest);

    RegisterResponseDTO register(RegisterRequestDTO registerRequestDTO);
}
