package com.bds.bds.enums;

public enum TokenType {
    BEARER,
    REFRESH,
    RESET_PASSWORD
}
