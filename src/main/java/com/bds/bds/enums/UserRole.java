package com.bds.bds.enums;

public enum UserRole {
    ADMIN,
    USER,
    USER_PREMIUM
}
