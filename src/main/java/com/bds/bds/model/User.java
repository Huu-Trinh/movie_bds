package com.bds.bds.model;

import com.bds.bds.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractAuditingEntity {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private UserRole role;
    private Date dob;
    private String phone;
    private String avatar;
    private Integer status;
}
