package com.bds.bds.model.dto.request;

import com.bds.bds.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequestDTO {
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private UserRole role;
}
