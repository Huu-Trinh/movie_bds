package com.bds.bds.model.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequestDTO {
    @NotBlank(message = "{common.not-blank.message}")
    private String username;
    @NotBlank(message = "{common.not-blank.message}")
    @Size(min = 6, message = "{common.size.message}")
    private String password;

}
