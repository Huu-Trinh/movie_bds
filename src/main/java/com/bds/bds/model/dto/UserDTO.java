package com.bds.bds.model.dto;

import com.bds.bds.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO {
    private Integer id;
    private String email;
    private String phone;
    private String username;
    private String firstName;
    private String lastName;
    private String avatar;
    private UserRole role;
}
